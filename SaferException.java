package my3.ua.cz.taras;

// Змінити клас так, щоб його можна було використовувати у якості винятка
public class SaferException extends Exception {
    public SaferException(String description) {
        super(description);

    }

}
