package my3.ua.cz.taras;

import my3.ua.cz.taras.SaferException;

public class ExceptionMaker {
    public static void main(String[] args) throws SaferException  {
        new ExceptionMaker().isThereTwo(getData());
    }
    public boolean isThereTwo(Object[] objectsArray) {
        try {
        for (Object someObject : objectsArray) {
            if (someObject.equals(2)) {
                return true;
            }
            }
        }
        catch (NullPointerException e) {
            new SaferException("Містить нуль");
        }
        return false;
    }
    public static Object[] getData() {
        return new Object[] { new Object(), null, "test data", 2 };
    }
}
